#pragma once

#include <paxos/node/proposal.hpp>

#include <whirl/cereal/serialize.hpp>

#include <optional>

namespace paxos {

namespace proto {

////////////////////////////////////////////////////////////////////////////////

// Phase I: Prepare / Promise

struct Prepare {
  struct Request {
    int32_t _;  // Your fields goes here

    WHIRL_SERIALIZE(_)
  };

  // Promise
  struct Response {
    int32_t _;

    WHIRL_SERIALIZE(_)
  };
};

////////////////////////////////////////////////////////////////////////////////

// Phase II: Accept / Accepted

struct Accept {
  struct Request {
    int32_t _;

    WHIRL_SERIALIZE(_)
  };

  struct Response {
    int32_t _;

    WHIRL_SERIALIZE(_)
  };
};

}  // namespace proto

}  // namespace paxos
