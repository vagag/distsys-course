#pragma once

#include <paxos/node/proposal.hpp>
#include <paxos/node/proto.hpp>

#include <whirl/node/logging.hpp>
#include <whirl/node/peer_base.hpp>
#include <whirl/rpc/use/service_base.hpp>

namespace paxos {

using whirl::NodeServices;
using whirl::PeerBase;
using whirl::rpc::ServiceBase;

////////////////////////////////////////////////////////////////////////////////

// Acceptor role / RPC service

class Acceptor : public ServiceBase<Acceptor>, public PeerBase {
 public:
  Acceptor(NodeServices runtime) : PeerBase(std::move(runtime)) {
  }

 protected:
  void RegisterRPCMethods() {
    // Phase I
    RPC_REGISTER_METHOD(Prepare);
    // Phase II
    // Your code goes here
  }

  proto::Prepare::Response Prepare(proto::Prepare::Request request) {
    proto::Prepare::Response response;
    // Your code goes here
    return response;
  }
};

}  // namespace paxos
