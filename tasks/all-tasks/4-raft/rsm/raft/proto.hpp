#pragma once

#include <rsm/raft/log_entry.hpp>
#include <rsm/raft/server_id.hpp>

#include <whirl/cereal/serialize.hpp>

#include <cstdlib>
#include <ostream>

namespace raft {

namespace proto {

//////////////////////////////////////////////////////////////////////

struct RequestVote {
  struct Request {
    size_t term;
    ServerId candidate_id;
    size_t last_log_index;
    size_t last_log_term;

    WHIRL_SERIALIZE(term, candidate_id, last_log_index, last_log_term)
  };

  struct Response {
    size_t term;
    bool vote_granted;

    WHIRL_SERIALIZE(term, vote_granted)
  };
};

//////////////////////////////////////////////////////////////////////

struct AppendEntries {
  struct Request {
    size_t term;
    ServerId leader_id;
    size_t prev_log_index;
    size_t prev_log_term;
    LogEntries entries;
    size_t leader_commit_index;

    WHIRL_SERIALIZE(term, leader_id, prev_log_index, prev_log_term, entries,
                    leader_commit_index)
  };

  struct Response {
    size_t term;
    bool success;
    size_t next_index_hint;

    WHIRL_SERIALIZE(term, success, next_index_hint)
  };
};

//////////////////////////////////////////////////////////////////////

// For logging

std::ostream& operator<<(std::ostream& out,
                         const RequestVote::Request& request);

std::ostream& operator<<(std::ostream& out,
                         const AppendEntries::Request& request);

}  // namespace proto

}  // namespace raft
