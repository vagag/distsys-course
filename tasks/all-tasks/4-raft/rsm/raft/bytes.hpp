#pragma once

#include <whirl/cereal/bytes.hpp>

namespace raft {

using whirl::Bytes;

}  // namespace raft
