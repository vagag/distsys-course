#pragma once

#include <rsm/raft/server_id.hpp>

#include <whirl/node/node_methods_base.hpp>

#include <whirl/rpc/use/channel.hpp>

#include <vector>
#include <map>

namespace raft {

class PeerBase : public whirl::NodeMethodsBase {
 protected:
  PeerBase(whirl::NodeServices runtime)
      : whirl::NodeMethodsBase(std::move(runtime)) {
  }

  void ConnectToPeers();

  size_t ClusterSize() const;

  ServerId MyId() const;

  /*
   * Usage:
   * for (auto id : PeerIds()) {
   *   PeerChannel(id).Call("RaftNode.AppendEntries", ...);  // Excluding
   * self
   * }
   */
  const std::vector<ServerId>& PeerIds() const;

  // Transport channel, no retries / timeouts
  whirl::rpc::TChannel PeerChannel(ServerId id);

  const std::string& PeerName(ServerId id) const;

 private:
  std::vector<ServerId> peer_ids_;
  std::map<ServerId, whirl::rpc::IChannelPtr> channels_;
};

}  // namespace raft
