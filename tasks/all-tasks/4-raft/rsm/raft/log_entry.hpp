#pragma once

#include <rsm/raft/command.hpp>

#include <whirl/cereal/serialize.hpp>

#include <vector>

#include <cereal/types/vector.hpp>

namespace raft {

struct LogEntry {
  Command command;
  size_t term;

  WHIRL_SERIALIZE(command, term)
};

using LogEntries = std::vector<LogEntry>;

}  // namespace raft
