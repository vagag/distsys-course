#pragma once

#include <rsm/raft/server_id.hpp>
#include <rsm/raft/request_id.hpp>
#include <rsm/raft/command.hpp>
#include <rsm/raft/rsm_response.hpp>

#include <whirl/rpc/use/channel.hpp>
#include <whirl/rpc/impl/client.hpp>
#include <whirl/rpc/impl/errors.hpp>

#include <whirl/node/services.hpp>
#include <whirl/node/logging.hpp>
#include <whirl/time.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>

#include <map>
#include <vector>

namespace raft {

using IRPCClientPtr = whirl::rpc::IClientPtr;
using whirl::INodeLoggerPtr;
using whirl::IRandomServicePtr;
using whirl::ITimeServicePtr;
using whirl::IUidGeneratorPtr;

using namespace whirl::time_literals;

using await::futures::Future;

//////////////////////////////////////////////////////////////////////

using ServerAddresses = std::vector<std::string>;

class BlockingClient {
 public:
  BlockingClient(ServerAddresses cluster, IRPCClientPtr rpc_client,
                 IUidGeneratorPtr uids, ITimeServicePtr time,
                 INodeLoggerPtr logger)
      : time_(std::move(time)), logger_(std::move(logger)) {
    GenerateClientId(uids);
    ConnectToRSM(cluster, rpc_client);
  }

 public:
  template <typename Op>
  typename Op::Response Execute(const std::string& op,
                                typename Op::Request request) {
    // Request for Raft RSM

    size_t attempt = 0;

    // Generate request id
    RequestId id = NextRequestId();

    // Serialize operation request to RAFT command
    Command command{id, op, Bytes::Serialize(request)};

    while (true) {
      ++attempt;

      // Cached leader?
      auto target_channel = TargetChannel();

      NODE_LOG("Send command {} to {} (id = {}) (attempt {})", command,
               target_channel.Peer(), target_id_, attempt);

      // RPC
      auto f = target_channel.Call("RaftRSM.Execute", command).As<RSMResponse>();

      // Await RPC result
      auto result = await::fibers::Await(std::move(f));

      if (!result.IsOk()) {
        // RPC failed

        // Extract error code
        const auto& error = result.GetError().GetErrorCode();

        if (IsRetriableRPCError(error)) {
          RotateTargetServer();
          continue;  // Retry on transport errors
        } else {
          WHEELS_PANIC("Raft internal error, command "
                       << command << " failed: " << error.message());
        }
      }

      RSMResponse rsm_response = result.Value();

      if (rsm_response.index() == 0) {
        // 1) Redirect to leader
        RedirectToLeader redirect_to = std::get<0>(rsm_response);
        CacheLeader(redirect_to.id);
        NODE_LOG("Command {} redirected to server {}", command, redirect_to.id);
      } else if (rsm_response.index() == 1) {
        // 2) Target server is not a leader
        NODE_LOG("{} is not a leader, switch to next target server",
                 target_id_);
        RotateTargetServer();
        Backoff(50_jiffies);
      } else {
        // 3) Operation response
        auto op_response_bytes = std::get<2>(rsm_response);
        return op_response_bytes.As<typename Op::Response>();
      }
    }
  }

 private:
  void GenerateClientId(const IUidGeneratorPtr uids) {
    client_id_ = uids->Generate();
  }

  void ConnectToRSM(const ServerAddresses& cluster, IRPCClientPtr rpc_client) {
    for (size_t i = 0; i < cluster.size(); ++i) {
      ServerId id = i + 1;  // Configuration
      auto channel = rpc_client->Dial(cluster[i]);
      servers_.emplace(id, channel);
    }
  }

  void CacheLeader(ServerId leader_id) {
    target_id_ = leader_id;
  }

  void RotateTargetServer() {
    ++target_id_;
    if (target_id_ > servers_.size()) {
      target_id_ = 1;
    }
    NODE_LOG("Rotate target server to {}", target_id_);
  }

  void Backoff(whirl::Duration delay) {
    await::fibers::Await(time_->After(delay)).ExpectOk();
  }

  whirl::rpc::TChannel TargetChannel() {
    return {servers_[target_id_]};
  }

  static bool IsRetriableRPCError(const std::error_code error) {
    return error == RPCErrorCode::TransportError;
  }

  raft::RequestId NextRequestId() {
    return {client_id_, ++request_index_};
  }

  const INodeLoggerPtr& NodeLogger() const {
    return logger_;
  }

 private:
  std::map<ServerId, whirl::rpc::IChannelPtr> servers_;
  ServerId target_id_{1};  // Target for next request

  std::string client_id_;
  size_t request_index_{0};

  ITimeServicePtr time_;  // For retries
  INodeLoggerPtr logger_;
};

}  // namespace raft
