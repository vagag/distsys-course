#include <rsm/raft/command.hpp>

namespace raft {

std::ostream& operator<<(std::ostream& out, const Command& command) {
  out << command.type << "/" << command.id;
  return out;
}

bool operator==(const Command& lhs, const Command& rhs) {
  return lhs.id == rhs.id;
}

bool operator!=(const Command& lhs, const Command& rhs) {
  return !(lhs == rhs);
}

}  // namespace raft
