#include <rsm/kv/proto/proto.hpp>

namespace kv {

namespace proto {

std::ostream& operator<<(std::ostream& out, const Set::Request& set) {
  out << "SetRequest(" << set.key << ", " << set.value << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const Get::Request& get) {
  out << "GetRequest(" << get.key << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const Cas::Request& cas) {
  out << "CasRequest(" << cas.key << ", " << cas.expected_value << ", "
      << cas.target_value << ")";
  return out;
}

}  // namespace proto

}  // namespace kv
