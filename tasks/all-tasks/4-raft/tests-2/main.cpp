#include <rsm/kv/node/node.hpp>
#include <rsm/kv/store/types.hpp>
#include <rsm/kv/model/model.hpp>

#include <whirl/node/node_base.hpp>
#include <whirl/node/logging.hpp>

// Simulation
#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/world/global/vars.hpp>
#include <whirl/matrix/test/random.hpp>
#include <whirl/matrix/test/reporter.hpp>
#include <whirl/matrix/world/global/random.hpp>
#include <whirl/matrix/world/global/time.hpp>
#include <whirl/matrix/adversary/adversary_base.hpp>
#include <whirl/matrix/memory/heap.hpp>

#include <rsm/sim/client_base.hpp>
#include <rsm/sim/world_behaviour.hpp>

#include <await/fibers/core/id.hpp>
#include <whirl/rpc/impl/id.hpp>

// Concurrency
#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>
#include <await/futures/combine/quorum.hpp>

#include <wheels/support/size_literals.hpp>

#include <cereal/types/string.hpp>
#include <fmt/ostream.h>

#include <random>

using namespace await::fibers;
using namespace whirl;
using namespace wheels::size_literals;

//////////////////////////////////////////////////////////////////////

class AtomicCounter {
 public:
  AtomicCounter(kv::BlockingClient& client, const std::string& name)
    : client_(client), key_(std::move(name)) {
  }

  // Lock-free
  size_t FetchAdd(size_t d) {
    while (true) {
      size_t current = Get();
      if (Cas(current, current + d) == current) {
        return current;
      }
    }
  }

  size_t Get() {
    return FromValue(client_.Get(key_));
  }

 private:
  size_t Cas(size_t expected, size_t target) {
    kv::Value prev_value = client_.Cas(key_, ToValue(expected), ToValue(target));
    return FromValue(prev_value);
  }

  static size_t FromValue(kv::Value value) {
    if (value == kv::ValueTraits::Default()) {
      return 0;
    }
    return std::stoi(value);
  }

  static kv::Value ToValue(size_t value) {
    if (value == 0) {
      return kv::ValueTraits::Default();
    }
    return std::to_string(value);
  }

 private:
  kv::BlockingClient& client_;
  std::string key_;
};

//////////////////////////////////////////////////////////////////////

class KVClient final : public KVClientBase {
 public:
  KVClient(NodeServices runtime) : KVClientBase(std::move(runtime)) {
  }

 protected:
  void MainThread() override {
    kv::BlockingClient kv_client = MakeKVClient();

    AtomicCounter counter(kv_client, "counter");

    const size_t increments_to_do = GetGlobal<size_t>("increments_per_client");

    for (size_t i = 0; i < increments_to_do; ++i) {
      size_t prev_value = counter.FetchAdd(1);

      GlobalCounter("increments").Increment();
      GlobalCounter("total").Add(prev_value);

      SleepFor(RandomNumber(1, 100));
    }

    NODE_LOG("Counter value after local increments: {}", counter.Get());
  }
};

//////////////////////////////////////////////////////////////////////

class Adversary : public adversary::AdversaryBase {
 public:
  Adversary(ThreadsRuntime runtime) : AdversaryBase(runtime) {
  }

 protected:
  void Initialize() {
    good_times_ = GlobalRandomNumber(5000, 10000);
  }

  void AbuseNetwork(IFaultyNetwork& net) {
    while (true) {
      if (NoMoreNetworkFaults()) {
        break;
      }

      RandomPause(50, 1000);
      net.Split();
      RandomPause(500, 1500);
      net.Heal();
    }
  }

  void AbuseServer(IFaultyServer& /*server*/) {
    // See AbuseWholeCluster
  }

  void AbuseWholeCluster(std::vector<IFaultyServer*> cluster) {
    while (true) {
      RandomPause(500, 1000);
      IFaultyServer* victim = RandomVictim(cluster);

      victim->Pause();
      WHIRL_SIM_LOG("Pause server {}", victim->Name());
      RandomPause(100, 1000);
      victim->Resume();
    }
  }

 private:
  IFaultyServer* RandomVictim(std::vector<IFaultyServer*> cluster) {
    return cluster[GlobalRandomNumber(cluster.size())];
  }

  bool NoMoreNetworkFaults() const {
    return GlobalNow() > good_times_;
  }

 private:
  TimePoint good_times_;
};

//////////////////////////////////////////////////////////////////////

TestReporter reporter;

//////////////////////////////////////////////////////////////////////

// Seed -> simulation digest
size_t RunSimulation(size_t seed) {
  static const size_t kTimeLimit = 30'000;

  reporter.PrintSimSeed(seed);

  Random random{seed};

  // Simulation parameters

  const size_t replicas = random.Get(3, 5);
  const size_t clients = random.Get(3, 5);
  const size_t increments_per_client = random.Get(2, 3);

  const size_t increments = clients * increments_per_client;

  reporter.DebugLine(
      "Parameters: replicas = {}, clients = {}, increments_per_client = {}",
      replicas, clients, increments_per_client);

  // Reset RPC and fiber ids
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Globals

  world.SetGlobal("increments_per_client", increments_per_client);

  world.InitCounter("increments");
  world.InitCounter("total");

  // World behaviour
  world.SetBehaviour(std::make_shared<whirl::RaftWorldBehaviour>());

  // Cluster nodes
  auto node = MakeNode<kv::Node>();
  world.AddServers(replicas, node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(clients, client);

  // Adversary
  world.SetAdversary(
      adversary::MakeStrategy<Adversary>());

  // Run simulation
  world.Start();

  while (world.GetCounter("increments") < increments &&
      world.TimeElapsed() < kTimeLimit) {
    if (!world.Step()) {
      break;
    }
  }

  // Stop and compute simulation digest
  size_t digest = world.Stop();

  const auto text_log = world.TextLog();

  // Print report
  reporter.PrintSimReport(world);

  const bool completed = world.GetCounter("increments") == increments;

  // Failed
  if (!completed) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(text_log);

    if (world.TimeElapsed() >= kTimeLimit) {
      reporter.Fail("Simulation time limit exceeded");
    } else {
      reporter.Fail("Deadlock in simulation");
    }
  }

  const size_t total = world.GetCounter("total");
  const size_t total_expected = increments * (0 + increments - 1) / 2;

  reporter.DebugLine("Total = {}, expected = {}",
      total, total_expected);

  const bool correct = total == total_expected;

  if (!correct) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    // Log
    reporter.PrintSimLog(text_log);
    // History
    reporter.PrintSimHistory<kv::Model>(world.History());
    reporter.Fail("Test invariant violated");
  }

  return digest;
}

void TestDeterminism() {
  static const size_t kSeed = 104107713;

  reporter.PrintLine("Test determinism...");

  size_t digest = RunSimulation(kSeed);

  // Repeat with the same seed
  if (RunSimulation(kSeed) != digest) {
    reporter.Fail("Impl is not deterministic");
  } else {
    reporter.PrintLine("Ok");
  }
}

void RunSimulations(size_t count) {
  std::mt19937 seeds{42};

  reporter.ResetSimCount();
  reporter.PrintLine("Run simulations...");

  for (size_t i = 1; i <= count; ++i) {
    reporter.StartSim();
    RunSimulation(seeds());
    reporter.CompleteSim();
  }
  reporter.DebugLine("Completed!");
}

int main() {
  whirl::SetHeapSize(1024_MiB);

  TestDeterminism();
  RunSimulations(7'000);

  reporter.Congratulate();

  return 0;
}
