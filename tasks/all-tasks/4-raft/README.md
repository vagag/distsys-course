# KV + RAFT = ♥

## Пролог

_After struggling with Paxos ourselves, we set out to find a new consensus algorithm that could provide a better foundation for system building and education_.

[Источник](https://raft.github.io/raft.pdf)

## Задача

Реализуйте отказоустойчивое линеаризуемое KV хранилище с помощью RSM + RAFT.

## Отказоустойчивость

Система должна переживать:

- Рестарты узлов
- Партишены сети
- Отказ произвольного меньшинства узлов

## Модель согласованности

[Линеаризуемость](https://jepsen.io/consistency/models/linearizable)

## RAFT

- [In Search of an Understandable Consensus Algorithm](https://raft.github.io/raft.pdf) – спецификация на 4-ой странице
- [RAFT lecture](https://www.youtube.com/watch?v=YbZ3zDzDnrw), [слайды](https://ongardie.net/static/raft/userstudy/raft.pdf)
- [TLA+ Spec](https://github.com/ongardie/raft.tla/blob/master/raft.tla)

#### Реализация

- [LogCabin](https://github.com/logcabin/logcabin) – референсная реализация от автора RAFT 
- [Implementing RAFT](https://eli.thegreenplace.net/2020/implementing-raft-part-0-introduction/) – учебная реализация на Go
- [Students' Guide to Raft](https://thesquareplanet.com/blog/students-guide-to-raft/)

## Фреймворк

### RAFT

- [`IStateMachine`](rsm/raft/state_machine.hpp) - реплицируемый автомат, реализация подставляется пользователем
- [`RSMResponse`](rsm/raft/rsm_response.hpp) – ответ реплики RSM клиенту: `RedirectToLeader` | `NotALeader` | `Bytes` (сериализованный response пользовательской операции)
- [`BlockingClient`](rsm/raft/client.hpp) – клиент, обрабатывает редиректы (`RedirectToLeader`) и отказы (`NotALeader`) от фолловеров / кандидатов, генерирует идентификаторы для команд для обеспечения семантики _exactly-once_.
- [`RequestId`](rsm/raft/request_id.hpp) – пара из `client_id` и `request_index`, для обеспечения семантики _exactly-once_
- [`Command`](rsm/raft/command.hpp) – сериализованный request пользовательской операции, снабженный идентификатором, отправляется клиентом
- [`PersistentLog`](rsm/raft/log.hpp) – персистентный лог, умеет 1) `Append` 2) `Truncate`
- [`proto::AppendEntries` и `proto::RequestVote`](rsm/raft/proto.hpp) – сообщения протокола RAFT

## Чего от меня хотят?

Реализовать метод [`RaftRSM::Execute`](rsm/raft/raft.cpp).

Никакие другие файлы менять не требуется.

## Замечания по реализации

### Роли и коммуникация

В _Multi-Paxos_ каждая реплика была и _Proposer_-ом, и _Acceptor_-ом, так что _Proposer_, проходя через фазы _Paxos_, всегда отправлял запросы в том числе и самому себе, локальному _Acceptor_-у. 

В RAFT роли взаимоисключающие: если реплика в даннвй момент является лидером, то она общается только с другими узлами-фолловерами. 

Для итерации по пирам реплики (исключая саму себя) используйте конструкцию
```cpp
for (ServerId peer_id : PeerIds()) {
    // ...
}
```

### RPC и ретраи

В простой реализации _Multi-Paxos_ внутри каждого слота алгоритм проходил через блокирующие фазы сборки кворумов. Поэтому удобно было поместить ретраи на уровень RPC под кворумный комбинатор `Quorum`.

В RAFT репликация лога на каждого из фолловеров происходит в своем собственном темпе. По этой причине ретраи на уровне RPC, которые были удобны в алгоритме _Paxos_, становятся ненужными: гораздо разумнее повторить неудачный `AppendEntries` на фолловера на уровне алгоритма, а не на уровне RPC, при этом отправив на фолловера больше записей из лога, которые накопились к моменту ретрая.

### Concurrency

Используйте подход из [Implementing RAFT](https://eli.thegreenplace.net/2020/implementing-raft-part-0-introduction/):

На любую конкурентную активность заводите отдельный файбер. Привязывайте каждый файбер к конкретному терму + роли. 

Каждая реплика в RAFT - автомат. Переходы между состояниями в этом автомате удобно выполнять под одним мьютексом.

Разумеется, если файбер синхронно ждет завершения RPC или какого-то события, то он должен на это время отпускать мьютекс. За время ожидания роль и/или терм реплики могли измениться, поэтому после повторного захвата мьютекса нужно проверить, соответствует ли актуальное состояние реплики ожидаемому.

### Ticker + Select

Лидер в RAFT отправляет фолловерам `AppendEntries` по двум событиям: 
- тик таймера (лидер обязан отправлять фолловерам регулярные хартбиты независимо от наличия у него новых команд от клиентов),
- появление новых команд в логе.

Для реализации [тикера](https://gobyexample.com/tickers) используйте канал ([`await::fibers::Channel<T>`](https://gitlab.com/Lipovsky/await/-/blob/master/await/fibers/sync/channel.hpp)) единичной емкости + неблокирующую операцию `TrySend`. В качестве типа сообщения для канала разумно использовать [`wheels::Unit`](https://gitlab.com/Lipovsky/wheels/-/blob/master/wheels/support/unit.hpp).

Мультиплексировать события из разных каналов можно с помощью [`await::fibers::Select`](https://gitlab.com/Lipovsky/await/-/blob/master/await/fibers/sync/select.hpp).

### Nop

В алгоритме _Multi-Paxos_ в логах могли возникать дырки, и для финализации соответствующих слотов можно было использовать служебную команду `Nop`.

В RAFT, в отличие от _Multi-Paxos_, дырок в логах не бывает, но тем не менее команда `Nop` тоже может быть полезна для достижения liveness. 
