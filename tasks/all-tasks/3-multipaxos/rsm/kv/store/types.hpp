#pragma once

#include <string>

namespace kv {

using Key = std::string;

using Value = std::string;

struct ValueTraits {
  static Value Default() {
    return "<>";
  }
};

}  // namespace kv
