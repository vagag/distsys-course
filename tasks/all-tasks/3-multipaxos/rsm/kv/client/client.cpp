#include <rsm/kv/client/client.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>

#include <fmt/core.h>

using await::fibers::Await;
using await::futures::Future;

namespace kv {

BlockingClient::BlockingClient(TChannel channel, const std::string& client_id)
: rpc_channel_(std::move(channel)), client_id_(client_id) {
}

void BlockingClient::Set(Key key, Value value) {
  auto id = NextRequestId();
  Future<void> future =
      rpc_channel_.Call("KV.Set", SetRequest{id, key, value});
  Await(std::move(future)).ExpectOk();
  return;
}

Value BlockingClient::Get(Key key) {
  auto id = NextRequestId();
  Future<Value> future = rpc_channel_.Call("KV.Get", GetRequest{id, key});
  return Await(std::move(future)).Value();
}

Value BlockingClient::Cas(Key key, Value expected, Value target) {
  auto id = NextRequestId();
  Future<Value> future =
      rpc_channel_.Call("KV.Cas", CasRequest{id, key, expected, target});
  return Await(std::move(future)).Value();
}

RequestId BlockingClient::NextRequestId() {
  return fmt::format("{}-{}", client_id_, ++request_index_);
}

}  // namespace kv
