#include <rsm/kv/client/client.hpp>
#include <rsm/kv/node/node.hpp>
#include <rsm/kv/store/types.hpp>
#include <rsm/kv/model/model.hpp>

#include <whirl/node/node_base.hpp>
#include <whirl/node/logging.hpp>

// Simulation
#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/history/checker/check.hpp>
#include <whirl/matrix/world/global/vars.hpp>
#include <whirl/matrix/world/global/random.hpp>
#include <whirl/matrix/world/global/time.hpp>
#include <whirl/matrix/adversary/adversary_base.hpp>
#include <whirl/matrix/memory/heap.hpp>

#include <whirl/matrix/test/random.hpp>
#include <whirl/matrix/test/reporter.hpp>

#include <await/fibers/core/id.hpp>
#include <whirl/rpc/impl/id.hpp>

// Concurrency
#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>

#include <wheels/support/size_literals.hpp>

#include <cereal/types/string.hpp>
#include <fmt/ostream.h>

#include <random>

using namespace wheels::size_literals;
using namespace await::fibers;
using namespace whirl;

//////////////////////////////////////////////////////////////////////

class KVClient final : public ExactlyOnceClientBase {
 public:
  KVClient(NodeServices runtime)
      : ExactlyOnceClientBase(std::move(runtime)) {
  }

 protected:
  void MainThread() override {
    kv::BlockingClient kv_client{Channel(), GenerateUid()};

    const size_t requests_to_do = GetGlobal<size_t>("requests_per_client");

    const kv::Key key = "test";

    for (size_t i = 0; i < requests_to_do; ++i) {
      if (RandomNumber() % 2 == 0) {
        kv::Value value = ToValue(RandomNumber(1, 100));
        NODE_LOG("Execute Set({}, {})", key, value);
        kv_client.Set(key, value);
        NODE_LOG("Set completed");
      } else {
        NODE_LOG("Execute Get({})", key);
        kv::Value result = kv_client.Get(key);
        NODE_LOG("Get({}) -> {}", key, result);
      }

      GlobalCounter("requests_done").Increment();

      SleepFor(RandomNumber(1, 100));
    }
  }

 private:
  static kv::Value ToValue(size_t integer) {
    return std::to_string(integer);
  }
};

//////////////////////////////////////////////////////////////////////

class WorldBehaviour : public IWorldBehaviour {
  // Time

  // [-25, +50]
  int InitClockDrift() override {
    return -25 + (int)GlobalRandomNumber(75);
  }

  int ClockDriftBound() override {
    return 75;
  }

  TimePoint GlobalStartTime() override {
    return GlobalRandomNumber(1000);
  }

  TimePoint ResetMonotonicClock() override {
    return GlobalRandomNumber(1, 100);
  }

  TimePoint InitLocalClockOffset() override {
    return GlobalRandomNumber(1000);
  }

  Duration TrueTimeUncertainty() override {
    return GlobalRandomNumber(5, 500);
  }

  // Network

  TimePoint FlightTime(const net::Packet& packet) override {
    if (packet.type != net::EPacketType::Data) {
      // Service packet, do not affect randomness
      return 50;
    }

    if (GlobalRandomNumber() % 5 == 0) {
      return GlobalRandomNumber(10, 1000);
    }
    return GlobalRandomNumber(30, 60);
  }
};

//////////////////////////////////////////////////////////////////////

class Adversary : public adversary::AdversaryBase {
 public:
  Adversary(ThreadsRuntime runtime) : AdversaryBase(runtime) {
  }

 protected:
  void Initialize() {
    good_times_ = GlobalRandomNumber(3000, 8000);
  }

  void AbuseNetwork(IFaultyNetwork& net) {
    while (true) {
      if (NoMoreFaults()) {
        break;
      }

      RandomPause(50, 3000);
      net.Split();
      RandomPause(50, 1000);
      net.Heal();
    }
  }

  void AbuseServer(IFaultyServer& server) {
    while (true) {
      if (NoMoreFaults()) {
        break;
      }

      RandomPause(300, 1500);
      if (server.IsAlive()) {
        server.FastReboot();
      }
    }
  }

  void AbuseWholeCluster(std::vector<IFaultyServer*> cluster) {
    size_t crashes = CrashesBudget();
    size_t size = cluster.size();

    while (crashes > 0) {
      if (NoMoreFaults()) {
        break;
      }

      RandomPause(100, 1000);

      if (GlobalRandomNumber() % 2 == 0) {
        if (crashes > 0) {
          crashes--;

          // Choose victim
          size_t victim = GlobalRandomNumber(size);

          std::swap(cluster[victim], cluster[size - 1]);
          size--;

          // Crash victim
          WHIRL_SIM_LOG("Explode server {}", cluster[size]->Name());
          cluster[size]->Crash();

          if (crashes == 0) {
            break;
          }
        }
      }
    }
  }

 private:
  size_t CrashesBudget() const {
    return GetGlobal<size_t>("crashes");
  }

  bool NoMoreFaults() const {
    return GlobalNow() > good_times_;
  }

 private:
  TimePoint good_times_;
};


//////////////////////////////////////////////////////////////////////

TestReporter reporter;

//////////////////////////////////////////////////////////////////////

// Seed -> simulation digest
size_t RunSimulation(size_t seed) {
  static const size_t kTimeLimit = 35'000;

  reporter.PrintSimSeed(seed);

  Random random{seed};

  // Simulation parameters

  const size_t replicas = random.Get(3, 5);
  const size_t clients = random.Get(3, 4);
  const size_t requests_per_client = random.Get(3, 4);

  const size_t total_requests = clients * requests_per_client;

  reporter.DebugLine(
      "Parameters: replicas = {}, clients = {}, requests_per_client = {}",
      replicas, clients, requests_per_client);

  const size_t crashes = random.Get(0, (replicas - 1) / 2);
  reporter.DebugLine("Crashes = {}", crashes);

  // Reset RPC and fiber ids
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Globals
  world.SetGlobal("requests_per_client", requests_per_client);
  world.SetGlobal("crashes", crashes);
  world.InitCounter("requests_done");

  // World behaviour
  world.SetBehaviour(std::make_shared<WorldBehaviour>());

  // Cluster nodes
  auto node = MakeNode<kv::Node>();
  world.AddServers(replicas, node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(clients, client);

  // Adversary
  world.SetAdversary(
      adversary::MakeStrategy<Adversary>());

  // Run simulation
  world.Start();
  while (world.GetCounter("requests_done") < total_requests &&
      world.TimeElapsed() < kTimeLimit) {
    if (!world.Step()) {
      break;
    }
  }

  // Stop and compute simulation digest
  size_t digest = world.Stop();

  const auto text_log = world.TextLog();

  // Print report
  reporter.PrintSimReport(world);

  // Time limit exceeded
  if (world.GetCounter("requests_done") < total_requests) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(text_log);

    if (world.TimeElapsed() >= kTimeLimit) {
      reporter.Fail("Simulation time limit exceeded");
    } else {
      reporter.Fail("Deadlock in simulation");
    }
  }

  // Check linearizability
  const auto history = world.History();

  reporter.DebugLine("History size: {}", history.size());

  const bool linearizable = histories::LinCheck<kv::Model>(history);

  if (!linearizable) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    // Log
    reporter.PrintSimLog(text_log);
    // History
    reporter.PrintLine("History (seed = {}) is NOT LINEARIZABLE:", seed);
    reporter.PrintSimHistory<kv::Model>(history);
    reporter.Fail();
  }

  return digest;
}

void TestDeterminism() {
  static const size_t kSeed = 104107713;

  reporter.PrintLine("Test determinism...");

  size_t digest = RunSimulation(kSeed);

  // Repeat with the same seed
  if (RunSimulation(kSeed) != digest) {
    reporter.Fail("Impl is not deterministic");
  } else {
    reporter.PrintLine("Ok");
  }
}

void RunSimulations(size_t count) {
  std::mt19937 seeds{42};

  reporter.ResetSimCount();
  reporter.PrintLine("Run simulations...");

  for (size_t i = 1; i <= count; ++i) {
    reporter.StartSim();
    RunSimulation(seeds());
    reporter.CompleteSim();
  }
}

int main() {
  // Increase default server heap size
  whirl::SetHeapSize(200_MiB);

  TestDeterminism();
  RunSimulations(3'000);

  reporter.Congratulate();

  return 0;
}
