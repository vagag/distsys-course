cmake_minimum_required(VERSION 3.14)

include(FetchContent)

# --------------------------------------------------------------------

# Offline mode (uncomment next line to enable)
# set(FETCHCONTENT_FULLY_DISCONNECTED ON)

# --------------------------------------------------------------------

# Libraries
set(FETCHCONTENT_QUIET OFF)

# --------------------------------------------------------------------

message(STATUS "FetchContent: twist")

FetchContent_Declare(
        twist
        GIT_REPOSITORY https://gitlab.com/Lipovsky/twist.git
        GIT_TAG master
)
FetchContent_MakeAvailable(twist)

# --------------------------------------------------------------------

message(STATUS "FetchContent: await")

FetchContent_Declare(
        await
        GIT_REPOSITORY https://gitlab.com/Lipovsky/await.git
        GIT_TAG master
)
FetchContent_MakeAvailable(await)

# --------------------------------------------------------------------

message(STATUS "FetchContent: whirl")

FetchContent_Declare(
        whirl
        GIT_REPOSITORY https://gitlab.com/Lipovsky/whirl.git
        GIT_TAG 4caa97e05b84c24677d98b01bc40c1db9f3027fa
)
FetchContent_MakeAvailable(whirl)